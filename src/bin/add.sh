#!/bin/bash

FOLDER="./data/"$(date +"%Y")

[ ! -d "$FOLDER" ] && mkdir -p "$FOLDER"

FILE=$(date +"%m")
TASK=$(date +"%Y-%m-%d|%H:%M:%S")

[ ! -f "$FOLDER/$FILE.csv" ] && MD=$(echo "$TASK"| md5sum -z | awk '{ print $1 }') && echo "$TASK|$MD|T|START LOG ######" > "$FOLDER/$FILE.csv"

# [ "$1" != "" ] && echo "$TASK|$1|$2" >> "$FOLDER/$FILE.csv"

if [ "$2" != "" ]; then
	str=$2
	t=$1
else
	str=$1
	t="T"
fi

MD=$(md5sum -z "$FOLDER/$FILE.csv" | awk '{ print $1 }')

echo "$TASK|$MD|$t|$str" >> "$FOLDER/$FILE.csv"