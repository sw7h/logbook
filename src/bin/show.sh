#!/bin/bash

if [ "$1" != "" ]; then
    n=$1
else
    n=-1
fi

find ./data/* -regextype posix-extended -regex ".*[0-9]{2}\.csv" -mtime $n -type f -exec cat {} +
# -exec cat {} +
# -exec grep '@' {} +
# | sed -e 's/@//'
